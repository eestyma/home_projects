from math import fabs
from math import *
import numpy as np

from math import factorial
def binomial(h, g):
    return factorial(h) // (factorial(g) * factorial(h - g))

def calculate_B(mi,ni_max):
        n=3
        n_1=2
        m=2
        l=2
        l_1=1
	delta=[]
	beta1=[]
	beta2=[]
	beta1_1=[]
	beta2_1=[]
	alfa1=[]
	alfa1_1=[]
	suma1=0
	suma2=0
	suma3=0
	suma4=0
	suma5=0

        for i in mi:

                        delta.append(i+ni_max[i]+l+l_1)
                        delta.append(i+n+m+n_1)
                        delta_minimum=min(delta)
                        delta_minimum=delta_minimum+1
                        delta=[]
                        beta1.append(0)
                        beta1.append(i-n_1-l)
                        beta1.append(ni_max[i]-n_1-l+m)
                        beta1_maximum=max(beta1)
                        beta1=[]
                        beta2.append(n-l)
                        beta2.append(ni_max[i])
                        beta2_minimum=min(beta2)
                        beta2=[]
                        beta1_1.append(0)
                        beta1_1.append(i-beta1_maximum-l-l_1)
                        beta1_1.append(ni_max[i]-beta1_maximum-l-l_1+m)
			beta1_1_maximum=max(beta1_1)
                        beta1_1=[]
                        beta2_1.append(n_1-l_1)
                        beta2_1.append(ni_max[i]-beta1_maximum)
                        beta2_1_minimum=min(beta2_1)
                        beta2_1=[]
                        alfa1.append(m)
                        alfa1.append(ni_max[i]-beta1_maximum-beta1_1_maximum-l_1+m)
                        alfa1_maximum=max(alfa1)
                        alfa1=[]
                        alfa1_1.append(m)
                        alfa1_1.append(ni_max[i]-beta1_maximum-beta1_1_maximum-alfa1_maximum+m)
                        alfa1_1_maximum=max(alfa1_1)
                        alfa1_1=[]
			Blist=[]
			for i in mi:
	        		for mm in range(beta1_maximum,beta2_minimum):
       	        	        	B_suma1=binomial((n-l),beta1_maximum)
               	        		suma1=suma1+B_suma1
				for nn in range(beta1_1_maximum,beta2_1_minimum):
                       			B_suma2=(binomial((n_1-l_1),beta1_1_maximum))*((factorial(delta_minimum))/(i+beta1_maximum+beta1_1_maximum+l+l_1+1))
                       			suma2=suma2+B_suma2
                		B=suma1*suma2
				Blist.append(B)
			return Blist
n=1
n_1=0
m=0
l=2
l_1=1

ni_2=n+n_1-m
ni=[]

mi=[]
mi_last=n+n_1
ni_max=[]
for i in range(mi_last+1):
        mi.append(i)
ni_zapis=[]
delta=[]
for i in mi:
        ni_zapis.append(0)
        ni_zapis.append(fabs(l-l_1)-i)
        ni_zapis.append(i-(l+l_1))
        maximum_ni=max(ni_zapis)
        ni_max.append(maximum_ni)
        ni_zapis=[]
        delta.append(i+ni_max[i]+l+l_1)
        delta.append(i+n+m+n_1)
        delta_minimum=min(delta)
        delta_minimum=delta_minimum+1

Be = calculate_B(mi, ni_max)
def calculate_A(Be,mi,ni_max):

	listaA=[]
	for i in mi:
                for y in Be:
                       A=(-1**(l+l_1))*((((2*l+l)*(2*l_1+l))*(binomial(l+m,m)*binomial(l_1+m,m))/binomial(l,m)*binomial(l_1,m)*binomial(2*n,n)*binomial(2*n_1,n_1))^(1/2))*(factorial(n+n_1-ni_max[i]))*(factorial(n)*factorial(n_1)*binomial(2*i,i)*factorial(delta[i]))^(-1)*y
                       listaA.append(A)
                return listaA

listaA=calculate_A(Be,mi,ni_max)
R=3
ksi=0.45
ksi_1=0.5
roa=R*ksi
rob=R*ksi_1
def calculate_g():
	G=[]
	for y in mi:
		for i in ni_max[y],ni_2:
			sumaG=((roa+rob)**(i))*(int(listaA[y]))
			G.append(sumaG)
	return G
g=calculate_g()
R=3
ksi=0.45
ksi_1=0.5
roa=R*ksi
rob=R*ksi_1

#Kalkulacja funkcji f
mi_maxf=n+n_1
def calculate_f(roa,rob,mi):
	f=[]
	if fabs(roa-rob) >= (-0.13+0.15*mi_last)*mi_last:
		f.append(0.5**(exp(-rob)+exp(-roa)))
		f.append((exp(-rob)-exp(-roa))/(roa-rob))
		for h in mi:
			if mi !=0 and mi !=-1:
				f.append(((4*(2*h+1)*(2*h-1))/((roa-rob)**2))*(f[-2]-f[-1]))
	else:
		t=int((2.5+5.76*abs((roa-rob))/mi_last))
		list_r=[]
		for i in range(mi+t+1):
        		list_r.append(0)
		for x in range(mi+t-1,0,-1):
        		r_mi=((1+((roa-rob)**2)/ (4*(2*mi+1)*(2*mi-1)))*list_r[x+1])**-1
        		list_r[x]=r_mi
		f.append(r_mi)
	return f
f=calculate_f(roa,rob,mi)		

def calculate_s(f,g):
	S=0
	const=((((2*roa)/(roa+rob)**(n+1)/2))*((2*rob)/(roa+rob))**(n_1)/2)
	for i in range(mi_last):
		su=(roa-rob)**i*f[i]*g[i]
		S=S+su
	return S

print calculate_s(f,g)

